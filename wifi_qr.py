import pyqrcode
import png

ssid = input("Voer Wi-Fi Netwerknaam in:")
password = input("Voer wachtwoord in:")

qr = "WIFI:T:WPA;S:"+ssid+";P:"+password+";;"

# debug print
# print(qr)


qr_code = pyqrcode.create(qr)
qr_code.png(ssid+".png", scale=5)

